*********************
Overview and Features
*********************

PETSc, the Portable, Extensible Toolkit for Scientific Computation,
is intended for use in large-scale application projects. PETSc includes a large suite of parallel linear, nonlinear
equation solvers and ODE integrators that are easily used in application codes written in
C, C++, Fortran and Python. PETSc provides many of the mechanisms needed within parallel
application codes, such as simple parallel matrix and vector assembly routines that allow
the overlap of communication and computation. In addition, PETSc includes support for
managing parallel PDE discretizations

.. toctree::
   :maxdepth: 2

   nutshell
   features
   gpu_roadmap
   matrix_table
   vector_table
   linear_solve_table
   nonlinear_solve_table
   tao_solve_table
   discrete_table
   ../docs/manual/index
   ../docs/manualpages/index
   ../docs/changes/index
   ../docs/manualpages/singleindex
